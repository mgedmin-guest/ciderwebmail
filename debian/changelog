ciderwebmail (1.05+20150729-5) unstable; urgency=medium

  * Simplify patch 1002, and stop (build-)depend on
    libmail-rfc822-address-perl.
    Thanks to Pali Rohár.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 30 Jun 2018 16:40:49 +0200

ciderwebmail (1.05+20150729-4) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ Jonas Smedegaard ]
  * Avoid broken module Email::Address:
    + Add patch 1001 to use Email::Address::XS (not Email::Address), and
      use Mail::RFC822::Address (not Regexp::Common::Email::Address).
    + (Build-)depend on libemail-address-xs-perl
      libmail-rfc822-address-perl (not libemail-address-xs-perl
      libregexp-common-email-address-perl).
    Closes: Bug#887535 (CVE-2015-7686). Thanks to Pali Rohár.
  * Declare compliance with Debian Policy 4.1.4.
  * Fix broken uWSGI symlink.
    Closes: Bug#877279. Thanks to Andreas Beckmann.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 30 Jun 2018 15:42:36 +0200

ciderwebmail (1.05+20150729-3) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/watch: fix syntax error and tarball name regexp.

  [ Jonas Smedegaard ]
  * Update watch file:
    + Fix use project-custom release prefix.
    + Fix use uversiomangle (not dversionmangle) for prerelease suffix.
    + Use substitution strings.
  * Modernize Vcs-* fields:
    + Consistently use git (not cgit) path.
    + Consistently include .git suffix in path.
  * Declare compliance with Debian Policy 4.1.0.
  * Update copyright info:
    + Use https protocol in file format URL.
    + Extend coverage for myself.
  * Drop obsolete lintian override regarding debhelper 9.
  * Tighten lintian overrides regarding License-Reference.
  * Modernize cdbs:
    + Do copyright-check in maintainer script (not during build).
    + Drop get-orig-source target: Use gbp import-orig --uscan.
    + Relax to build-depend unversioned on cdbs.
    + Stop build-depend on licensecheck libregexp-assemble-perl
      libimage-exiftool-perl libfont-ttf-perl.
  * Update package relations: Relax to (build-)depend unversioned on
    libmoosex-role-withoverloading-perl libcatalyst-perl
    libcatalyst-plugin-configloader-perl
    libcatalyst-plugin-static-simple-perl
    libcatalyst-plugin-stacktrace-perl
    libcatalyst-plugin-authentication-perl
    libcatalyst-plugin-session-store-fastmmap-perl
    libcatalyst-plugin-session-state-cookie-perl
    libcatalyst-model-dbic-schema-perl
    libcatalyst-action-renderview-perl
    libcatalyst-action-renderview-perl libparse-recdescent-perl
    libmail-imapclient-perl libhtml-defang-perl: Needed version
    satisfied even in oldstable.
  * Improve Apache config snippet to catch URL without trailing slash
    (unhandled by uWSGI).
    Closes: Bug#798610. Thanks to James Valleroy.
  * Symlink Apache config snippet to /etc/apache2/conf-available.
    Thanks to James Valleroy.
  * Modernize README.Debian slightly.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 21 Sep 2017 10:39:22 +0200

ciderwebmail (1.05+20150729-2) unstable; urgency=medium

  * Mark package as autopkg-testable.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 03 Sep 2016 17:55:57 +0200

ciderwebmail (1.05+20150729-1) unstable; urgency=medium

  [ upstream ]
  * Git snapshot.
    + Check template instead of locale folder for supported languages.
    + Remove more mootoolsisms.
    + Remove mootools form validation.
    + Make sure the sending progress dialog is removed after sending.
    + Remove mootools data validatoin from html template.
    + Replace JSON.decode from mootools with JSON.parse.
    + Add html5 input validation and replace CSS for composer.
    + Fix textarea scrolling in message composing.

  [ Jonas Smedegaard ]
  * Fix avoid license-mine .ico files (unsupported by exiftool - see
    <https://rt.cpan.org/Ticket/Display.html?id=114260>).
  * Modernize git-buildpackage config:
    + Avoid git- prefix.
    + Filter any .gitignore file.
  * Drop patch 1001: Adopted upstream.
  * Unfuzz patch 1002.
  * Update copyright info:
    + Extend coverage of packaging to include recent years.
  * Use recet licensecheck, and license-miner now shipped with CDBS.
    + Stop include license-miner locally (shipped with CDBS now).
    + Tighten to build-depend versioned on cdbs.
    + Build-depend on licensecheck (not devscripts).

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 03 Sep 2016 16:48:02 +0200

ciderwebmail (1.05-6) unstable; urgency=medium

  [ Axel Beckert ]
  * debian/copyright: Drop CiderWebmail's Request Tracker from
    Upstream-Contact. Seems to no more exist. Thanks DUCK!

  [ Jonas Smedegaard ]
  * Declare compliance with Debian Policy 3.9.8.
  * Use https protocol in Vcs-Git URL.
  * Update copyright info:
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.
  * Update watch file:
    + Bump file format to version 4.
    + Track only github source, and refine pattern.
    + Mangle download filename.
    + Add usage comment.
  * Include and use local script license-miner (succeeding
    print-metadata.pl).
  * Update package relations:
    + Build-depend on libfont-ttf-perl.
    + Stop build-depend on libipc-system-simple-perl lcdf-typetools.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 09 May 2016 15:21:01 +0200

ciderwebmail (1.05-5) unstable; urgency=medium

  * Team upload.
  * Fix symlink_to_dir call in debian/ciderwebmail.maintscript.
    + Add a trailing slash to the old-target path.
    + Bump prior-version.

    Thanks to Andreas Beckmann for the pointer and patch. (Closes: #774862)

 -- gregor herrmann <gregoa@debian.org>  Wed, 21 Jan 2015 18:19:25 +0100

ciderwebmail (1.05-4) unstable; urgency=medium

  * Team upload.
  * Handle symlink-to-directory transition of
    /usr/share/ciderwebmail/root/static/images/mimeicons.

    Add debian/ciderwebmail.maintscript with the symlink_to_dir instruction,
    and 'Pre-Depends: ${misc:Pre-Depends}' in debian/control{,.in}.

    Thanks to Andreas Beckmann for the bug report. (Closes: #774862)

 -- gregor herrmann <gregoa@debian.org>  Mon, 12 Jan 2015 18:00:22 +0100

ciderwebmail (1.05-3) unstable; urgency=medium

  * Add patch 1002 to fix submit button for sending mail.
    Closes: bug#773207.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 15 Dec 2014 17:10:44 +0100

ciderwebmail (1.05-2) unstable; urgency=medium

  * Add patch 1001 to fix resolve supported languages.
  * Fix strip more upstream install noise.
  * Fix build-depend on libcatalyst-devel-perl (due to upstream release
    done without generating embedded code copies below inc/ as required
    by Catalyst sometimes - e.g. on backports to Wheezy).

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 25 Oct 2014 18:49:13 +0200

ciderwebmail (1.05-1) unstable; urgency=low

  [ upstream ]
  * New release.
    + Now works when using Courier IMAP server.
    + Various improvements for the test suite.
    + Remove Mootools, replace with plain JavaScript.
    + HTML5 message submission progress bar.
    + Various html/css/design fixes across all browsers.
    + Basic managesieve support.
    + Improve text/html mail rendering support based on
      HTML::Defang and HTML::Tidy
    + Add support for Content Security Policy when rendering text/html
      parts.
    + New russian, czech and danish translations.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to use cgit web frontend.

  [ Jonas Smedegaard ]
  * Handle new Managesieve support:
    + Recommend libauthen-sasl-perl, for secure Managesieve connections.
    + Add managesieve options to Debian-forked configfile.
    + (Build-)depend on libnet-managesieve-perl.
  * Improve language support:
    + Add patch 1001 with danish localization.
    + Regenerate localized templates during build.
    + Build-depend on gettext.
  * Avoid installing unused locale or var files, or empty /usr/bin dir.
  * Adapt to upstream now using Oxygen icon theme also for mimeicons:
    + Stop recommend http-icons.
    + Drop http-icons paragraphs in copyright file.
    + Update Files paragraph about Oxygen icons in copyright file.
    + Add TODO about use of separately packaged oxygen-icon-theme.
  * Replace mootools-more.js (in addition to mootools-core.js) with
    symlink to separately packaged file.
  * Unfuzz patch 2001.
  * Update package relations (in addition to others above):
    + (Build-)depend on libhtml-defang-perl (not libhtml-scrubber-perl).
    + (Build-)depend on libjson-xs-perl, libtry-tiny-smartcatch-perl,
    + liblog-dispatch-perl, liblog-dispatch-config-perl,
      libcatalyst-plugin-log-dispatch-perl.
    + (Build-) depend on (recent libcatalyst-perl or)
      libcatalyst-plugin-unicode-encoding-perl (not
      libcatalyst-plugin-unicode-perl).
    + Tighten (build-)dependency on libmail-imapclient-perl.
  * Declare compliance with Debian Policy 3.9.6.
  * Add alternative git source URLs.
  * Drop patch 1001, and drop custom cleanup: Danish l10n now upstream.
  * Resolve localizations during build (don't hardcode).
  * Suppress .mo files in copyright check.
  * Extract metadata from images for copyright check (instead of
    skipping them).
    Build-depend on libimage-exiftool-perl, libregexp-assemble-perl,
    libipc-system-simple-perl and lcdf-typetools.
  * Update and simplify generating l10n files.
  * Stop recommend or add symlink for mootools: No longer used.
  * Fix declare $HOME when creating user.
    Thanks to lintian.
  * Drop excess files bogusly handled by upstream install routines.
    Thanks to lintian.
  * Use tagged releases at Github, and stop track checksum of upstream
    tarball.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 25 Oct 2014 02:35:27 +0200

ciderwebmail (1.04-3) unstable; urgency=medium

  * Update copyright info:
    + Fix revert adding short licensing texts: Wrong to silence lintian
      warning by inventing licensing statements!
    + Fix use comment and license pseudo-sections to obey silly
      restrictions of copyright format 1.0.
    + Update alternate sources, and add alternate upstream contact.
    + Fix add License section for LGPL-2.1+.
    + Extend and improve coverage of images.
    + Fix indent a license.
    + Extend coverage of packaging, and bump its licensing to GPL-3+.
  * Bump debhelper compatibility level to 8: debhelper 8 is available
    even in oldstable.
  * Update package relations:
    + Add fallback (build-)dependencies on older
      libcatalyst-modules-perl, to ease backporting.
    + Relax to recommend (not depend on) libdbd-sqlite3-perl.
    + Fix stop (build-)depending on libparent-perl: Provided by
      perl(-modules).
    + Build-depend on devscripts: Needed for CDBS copyright check.
  * Fix typo in PSGI file.
  * Provide nearly-out-of-the-box config for Apache 2 + uWSGI:
    + Create/remove default db dir in package scripts.
    + Install uWSGI and Apache snippets below /etc (not as example
      files).
    + Improve uWSGI config to run as separate user.
    + Improve uWSGI config to set HOME and CONFIG paths in PSGI script
      (not configfile), and list only commented out in uWSGI ini-file.
    + Tune uWSGI options for better production use.
    + Rewrite README.Debian to reflect improved setup.
  * Modernize get-orig-source hints for new github.com ABI.
  * Have git-import-orig suppress upstream .gitignore file.
  * Add README.source documenting use of git-buildpackage and CDBS.
  * Add alternate github URL to watch file.

 -- Jonas Smedegaard <dr@jones.dk>  Wed, 21 May 2014 14:24:37 +0200

ciderwebmail (1.04-2) unstable; urgency=low

  * Team upload

  [ gregor herrmann ]
  * Fix spelling error in package description. Thanks to Chad Dunlap for
    reporting the bug and forwarding the Ubuntu patch. (Closes: #684785)

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)

  [ Damyan Ivanov ]
  * use canonical Vcs-* URLs in control.in too
  * replace libcatalyst-modules-perl in (build-)dependencies with individual
    module packages
  * drop trailing slash from a directory symlink
  * do not ship empty /usr/bin
  * copyright:
    + point to GPL-1 explicitly
    + add short licensing texts for Artistic and GPL-1+
  * declare conformance with Policy 3.9.5

 -- Damyan Ivanov <dmn@debian.org>  Tue, 20 May 2014 10:20:09 +0300

ciderwebmail (1.04-1) unstable; urgency=low

  * New upstream release.

  * Fix depend on libdbd-sqlite3-perl.
  * Set default config dir in PSGI file.
  * Pass app arguments in PSGI file.
  * Reorganize configfile to closer match upstream default file.
  * Fix add 'send' section to config file, and recommend MTA.
  * Relax to suggest (not (build-)depend on) libfcgi-perl and
    libfcgi-procmanager-perl: Used only is example script.
  * Relax to recommend (not (build-)depend on) libjs-mootools.
  * Fix Vcs-* URLs to use pkg-perl group (not collab-maint).
  * Suggest dovecot (with virtual imap-server as fallback) and
    imapproxy.
  * Add Dovecot recommendation to long description.
  * Drop patch 1001: Adopted upstream.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 23 Jun 2012 05:02:02 +0200

ciderwebmail (1.04~20120610-2) unstable; urgency=low

  * Initial packaging release for unstable.
  * Fix adjust symlink to mootools.
  * Fix syntac of PSGI (Catalyst documentation is wrong).
  * Add README.Debian documenting ways to deploy.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 10 Jun 2012 22:29:53 +0200

ciderwebmail (1.04~20120610-1) experimental; urgency=low

  * New snapshot of upstream VCS.
    + Move common code to CiderWebmail::Test and fix Test::XPath
      dependency.
    + Make sure the reply_to array actually contains addresses.
    + Remove user_settings.sql from the repo and auto create it.
    + Use ->charset instead of passing charset arguments around.
    + Fix Perl::Critic warnings.
    + Output better warnings when unable to convert a string to utf8.
    + Add user_settings.sql to .gitignore.

  * Fix adjust symlink to http-icons.
  * Install default config as example file.
  * Install update_schema script as example file.
  * Install configfiles and sample uWSGI/Apache2 integration files.

 -- Jonas Smedegaard <dr@jones.dk>  Sun, 10 Jun 2012 20:11:58 +0200

ciderwebmail (1.04~20120607-1) experimental; urgency=low

  * Initial release.
    Closes: Bug#673342.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 08 Jun 2012 22:28:26 +0200

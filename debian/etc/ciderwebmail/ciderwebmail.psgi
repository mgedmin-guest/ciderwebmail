use strict;
use warnings;

BEGIN {
	$ENV{CIDERWEBMAIL_HOME} |= '/usr/share/ciderwebmail';
	$ENV{CIDERWEBMAIL_CONFIG} |= '/etc/ciderwebmail';
}

use CiderWebmail;

my $app = CiderWebmail->apply_default_middlewares(CiderWebmail->psgi_app(@_));
